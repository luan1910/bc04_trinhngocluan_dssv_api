function SinhVien(name, id, password, email, math, physics, chemistry) {
  this.name = name;
  this.id = id;
  this.password = password;
  this.email = email;
  this.math = math;
  this.physics = physics;
  this.chemistry = chemistry;
  this.tinhDTB = function () {
    return (this.math * 1 + this.physics * 1 + this.chemistry * 1) / 3;
  };
}
