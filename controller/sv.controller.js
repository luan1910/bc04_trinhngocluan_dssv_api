renderDSSV = function (dssv) {
  var contentHTML = "";
  dssv.forEach((sv, index) => {
    var contentTr = `<tr>
<td>${sv.id}</td>
<td>${sv.name}</td>
<td>${sv.email}</td>
<td>${0}</td>
<td>
<button onclick=xoaSinhVien('${sv.id}') class="btn btn-danger">Xóa</button>
<button onclick=suaSinhVien('${sv.id}') class="btn btn-warning">Sửa</button>
</td>
</tr>`;
    contentHTML += contentTr;
  });
  console.log("contentHTML: ", contentHTML);
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(tenSv, maSv, matKhau, email, diemToan, diemLy, diemHoa);
}
function timKiemViTri(id, newSv) {
  for (var index = 0; index < newSv.length; index++) {
    var sv = newSv[index];
    if (sv.id == id) {
      return index;
    }
  }
  return -1;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.math;
  document.getElementById("txtDiemLy").value = sv.physics;
  document.getElementById("txtDiemHoa").value = sv.chemistry;
}
